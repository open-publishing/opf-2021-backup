---
layout: singlepage.njk
title: "Event Toolkit"
class: "toolkit"
tags: 
  - toolkit
permalink: /toolkit.html
---

Here we present some helpful tips on avoiding zoom bombing, various conferencing/streaming technologies, fest chat, and how to promote events.

## Fest chat
Feel free to join the Festival chat channel. We are using the Coko Mattermost chat. You can create an account here:
[https://mattermost.coko.foundation](https://mattermost.coko.foundation)

Once logged in join the fest channel: [https://mattermost.coko.foundation/coko/channels/open-publishing-fest-chat](https://mattermost.coko.foundation/coko/channels/open-publishing-fest-chat)

The chat is for help with any technical issues etc but you can also use it just for hanging out. Feel free to share with others.

## Request changes to your event
Any changes to your event please use the change request form: [https://openpublishingfest.org/updateEvent.html](https://openpublishingfest.org/updateEvent.html)


## Video Conferencing / Streaming 
### **Don't have a platform?**
We can help with Big Blue Button (see below). Use the form for changes to request BBB or jump into the chat channel and ask for help.

If, however, you would like some guidance on tools and workflows for holding and archiving your event at Open Publishing Fest, we are happy to provide the following recommendations:

### **BigBlueButton** [https://bigbluebutton.org/](https://bigbluebutton.org/)

This open source option for video conferencing features a shared notepad, whiteboard, and breakout rooms for enhanced presentations, as well as simple recording and archiving capabilities. <a href="https://cloud68.co/">Cloud68</a> will facilitate a unique instance of Big Blue Button for Open Publishing Fest. Contact us on our Mattermost chat (info below) and we can assist you in setting this up.

### **Jitsi** [https://jitsi.org](https://jitsi.org)

An open source, secure tools to host a video conference with multiple participants. Schedule and host a free meeting by going to: <a href="https://meet.jit.si">https://meet.jit.si</a> or 8x8 - <a href="https://8x8.vc">https://8x8.vc</a> (An open source video conference client built on Jitsi).

### **YouTube** [https://www.youtube.com](https://www.youtube.com)

This ubiquitous video site has a number of options for streaming live or pre-recorded video to your audiences.

Note that many video conferencing tools have streaming capabilities through YouTube. For example check out 8x8’s <a href="https://support.8x8.com/cloud-phone-service/meetings/8x8-video-meetings-virtual-office/how-to-set-up-8x8-video-meetings-for-live-streaming-on-youtube">guide to streaming a video conference to YouTube</a>

For pre-recorded video, <a href="https://www.staff.universiteitleiden.nl/vr/video-toolkit?cf=science&amp;cd=fnw-board-office">Leiden University has a useful guide.</a>

## Tips on avoiding bombing
Last year we had one event bombed (out of 120). This year the fest has attracted more attention so we can expect someone will try to bomb an event or events. Most likely they will bomb zoom as it is the most common and popular platform.

### **Tips on Avoiding Bombing on Zoom**
* have someone dedicated to managing participants
* disable screen sharing for non-hosts
* mute participants at the start
* disable file transfer in chat

Also good:
* create a waiting room

Zoom actually has pretty good documentation on the items above (and more)... have a read : 
[https://support.zoom.us/hc/en-us/articles/115005759423-Managing-participants-in-a-meeting](https://support.zoom.us/hc/en-us/articles/115005759423-Managing-participants-in-a-meeting)

## Upload Archives of your event
If you archive/record your event please upload it to the Fest folder and we will add it to the archives so others can see it! [https://openpublishingfest-storage.cloud68.co/s/439WT8s2cbZdHGk](https://openpublishingfest-storage.cloud68.co/s/439WT8s2cbZdHGk)

## Promote your event, promote others!
Please promote your event through whatever channels are available to you. If you want to use some imagery from the fest feel free to use anything from the comms page: [https://openpublishingfest.org/comms.html](https://openpublishingfest.org/comms.html)

If possible use the hashtag **#OpenPublish**. Also please promote other fest events you see. One source for other events can be found on Twitter under the OpenPublish hashtag: [https://twitter.com/search?q=%23OpenPublish](https://twitter.com/search?q=%23OpenPublish).

The more we all promote each others event the larger the audience, the more folks see all of our work! Promote yourself, promote others!

## Final Event
Please consider joining the final event: [https://openpublishingfest.org/calendar.html#event-39/](https://openpublishingfest.org/calendar.html#event-39/)

The amazing Divine Divas (pic below) are performing again this year and it should be a blast :) 

![alt text](images/comms/TwitterEvent02.jpg "Divine Divas")





